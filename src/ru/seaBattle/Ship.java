package ru.seaBattle;
import java.util.ArrayList;

class Ship {
    private ArrayList<Integer> location = new ArrayList<>();

    Ship(int start, int shipLength) {
        for (int i = start; i < start + shipLength; i++) {
            location.add(i);
        }
    }

    @Override
    public String toString() {
        return "Корабль (" +
                "Местоположение - " + location +
                ')';
    }

    String shot(int shot) {
        String message = "Мимо";
        if (location.contains(shot)) {
            location.remove((Integer) shot);
            message = "Пробитие";
        }
        if (location.isEmpty()) {
            message = "Потоплен!";
        }
        return message;
    }

}





