package ru.seaBattle;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        /**
         * задаем рандомные значения длинны корабля и его координат в шлюзе
         * создаем объект Ship
         */
        int leighGateway = (int) (5 + Math.random() * 15);
        int shipLength = (int) (1 + Math.random() * 4);
        int start = (int) (1 + Math.random() * leighGateway - shipLength);
        System.out.println("Длина шлюза - " + (leighGateway));
        Ship ship = new Ship(start, shipLength);
       //      System.out.println("Ship" + ship + start + " " + shipLength);
        shots(ship);
    }

    /**
     * метод для выстрелов по кораблю
     */
    private static void shots( Ship ship) {
        int x = 0;
        int shot;
        do {
            System.out.println("Введите координату выстрела: ");
            shot = scanner.nextInt();
            System.out.println(ship.shot(shot));
            x++;
        } while (!ship.shot(shot).equals("Корабль потоплен!"));
        System.out.println("Вы потопили корабль с " + x + " попытки!");
    }


}
