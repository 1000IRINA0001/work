package ru;

import java.util.Scanner;

public class Palindrome {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("введите слово или фразу: ");
        String s = scanner.next();
        s.replaceAll("[^A-Za-zА-Яа-я0-9]", "").toLowerCase();
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) {
            System.out.println("палиндром");
        } else {
            System.out.println("не палиндром");
        }

    }
}
