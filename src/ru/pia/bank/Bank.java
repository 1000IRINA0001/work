package ru.pia.bank;

public class Bank {
    private double balance;
    private double interestRate;

    public Bank(double balance, double interestRate) {
        this.balance = balance;
        this.interestRate = interestRate;
    }

    public double getBalance() {
        return balance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "balance=" + balance +
                ", interestRate=" + interestRate +
                '}';
    }

    /**
     * метод, в котором вычисляется новый счет(счет с начислением процентов)
     * @return возвращаем значение нового счета
     */
    public double operation() {

        double Prozent = balance / 100 * interestRate;
        double newBalance=balance + Prozent;
        return newBalance;


    }
}