package ru.pia.patient;
import java.util.Scanner;

import java.util.ArrayList;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * в этом методе созданы исходные данные
     * создан ArrayList Patient и заполнен исходными данными

     */
    public static void main(String[] args) {
        Patient patient1= new Patient("Роман",1999,201664,true);
        Patient patient2= new Patient("Степа",2000,456879,false);
        Patient patient3= new Patient("Паша",1997,895232,true);
        Patient patient4= new Patient("Саша",1990,124564,false);
        Patient patient5= new Patient("Ирина",1996,324565,true);
        System.out.print("Сколько пациентов вы хотите ввести: ");
        int num =scanner.nextInt();
        ArrayList <Patient> patients = new ArrayList<>();
        for (int i = 0; i < num ; i++) {
            patients.add(input());
        }
        patients.add(patient1);
        patients.add(patient2);
        patients.add(patient3);
        patients.add(patient4);
        patients.add(patient5);
        Years ( patients);
    }

    /**
     * метод для ввода данных о пациенте
     * @return возвращаем объект о пациенте
     */
    private static Patient input(){
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.println("Введите год рождения: ");
        int year = scanner.nextInt();
        System.out.print("Введите номер карточки : ");
        int number = scanner.nextInt();
        System.out.print("Прошел ли пациент диспанцеризацию?: ");
        scanner.nextLine();
        boolean dispensary = scanner.nextBoolean();
        return new Patient(surname, year, number, dispensary );
    }

    /**
     * метод, в котором обрабатывается массив, и находятся и выводятся пациеты, старше 2000г.
     * @param patients
     */
    private static void Years (ArrayList <Patient> patients){
        int patientsSize = patients.size();
        for(int i = 0; i < patientsSize; i++) {
            int year = patients.get(i).getYear();
            boolean dis = patients.get(i).getDispensary();
            if (year < 2000) {
                if(dis==true){
                    System.out.print(patients.get(i));
                }
            }
        }
    }

}