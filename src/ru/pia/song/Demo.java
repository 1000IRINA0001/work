package ru.pia.song;
import java.util.ArrayList;
import java.util.Scanner;

public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * исходные данные
     */
    public static void main(String[] args) {
        Song song1 = new Song("beat it", "lil peep", 202);
        Song song2 = new Song("king", "xxxtentacion", 112);
        Song song3 = new Song("affection", "scruffpuppie", 244);
        Song song4 = new Song("авиарежим", "куок", 244);
        Song song5 = new Song("папа", "marco-9", 194);
/**
 * в ArrayList заносятся исходные данные
 */
        ArrayList<Song> songs = new ArrayList<>();
        songs.add(song1);
        songs.add(song2);
        songs.add(song3);
        songs.add(song4);
        songs.add(song5);
        System.out.println("Введите кол-во песен: ");
        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            songs.add(input());
        }
        songsShort(songs);
        System.out.println("Что бы вывести сведения о песнях,введите их продолжительность: ");
        int dur = scanner.nextInt();
        DurSongs(songs, dur);
        printSongs(songs);
    }

    /**
     * метод, в котором вводится с консоли параметры для песни, затем создается объект Song
     * @return
     */
    private static Song input() {
        System.out.println("Введите название трека: ");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.println("Введите исполнителя: ");
        String executor = scanner.nextLine();
        System.out.println("Введите длительность трека: ");
        int duration = scanner.nextInt();
        return new Song(name, executor, duration);
    }

    /**
     *
     * @param songs массив песен
     */
    private static void DurSongs(ArrayList<Song> songs, int dur) {
        int songSize = songs.size();
        for (int i = 0; i < songSize; i++) {
            if (dur == songs.get(i).getDuration()) {
                System.out.println(songs.get(i));
            }
        }
    }

    /**
     * обрабатывает массив песен и сравнивает категории 1 и 5 песен
     */
    private static void printSongs(ArrayList<Song> songs) {
        int songSize = songs.size();
        if (songs.get(0).isSameCategory(songs.get(songSize-1))) {
            System.out.println("У песни- " + songs.get(0).getName() + "- и песни -" + songs.get(songSize-1).getName() + " -одинаковые категории");
        }else {
            System.out.println("У песни- " + songs.get(0).getName() + "- и песни -" + songs.get(songSize-1).getName() + " -разные категории");
        }
    }

    /**
     * метод обрабатывает массив и ищет песни категории short; выводит эти пести
     * @param songs
     */
    private static void songsShort(ArrayList<Song> songs) {
        int songSize = songs.size();
        for (int i = 0; i < songSize; i++) {
            if (songs.get(i).getDuration() < 120) {
                System.out.println("Песня - " + songs.get(i).getName() + " - относится к short \n" + songs.get(i));

            }
        }
    }
}