package ru.pia.robot;

public enum Direction {
    UP,
    LEFT,
    RIGHT,
    DOWN
}
