package ru.pia.baggage;
import java.util.ArrayList;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * метод для ввода пассажиров. Создан arrayList Baggage
     */
    public static void main(String[] args) {
        System.out.print("Сколько пассажиров вы хотите ввести: ");
        int num = scanner.nextInt();
        ArrayList<Baggage> baggages = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            baggages.add(input());
        }
        pasClad(baggages);
    }

    /**
     * Инициализируем переменные: surname, number, weight
     * возвращаем объект Baggage
     * @return
     */
    private static Baggage input() {
        System.out.print("Введите фамилию: ");
        scanner.nextLine();
        String surname = scanner.nextLine();
        System.out.println("Введите количество багажных мест: ");
        int number = scanner.nextInt();
        System.out.print("Общий вес багажа : ");
        double weight = scanner.nextDouble();
        return new Baggage(surname, number, weight);
    }

    /**
     * ищет багаж С ручной кладью
     * @param baggages массив багажа
     */
    private static void pasClad(ArrayList<Baggage> baggages) {
        int baggagesSize = baggages.size();
        for (int i = 0; i < baggagesSize; i++) {
            if (baggages.get(i).Сlad()) {
                System.out.println(baggages.get(i).getSurname() + " с ручной кладью");
            }
        }
    }
}