package ru.pia.cat;

public class Cat {
    private String name;
    private Gender gender;
    private int year;

    public Cat(String name, Gender gender, int year) {
        this.name = name;
        this.gender = gender;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", gender=" + gender +
                ", year='" + year + '\'' +
                '}';
    }
}
