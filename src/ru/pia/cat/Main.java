package ru.pia.cat;

import java.util.Scanner;
import java.util.ArrayList;
public class Main {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Cat cat1 = new Cat("Барсик", Gender.MAN, 2012);
        Cat cat2 = new Cat("Мурка", Gender.WOMAN, 2012);
        Cat cat3 = new Cat("Кузя", Gender.MAN, 2012);
        Cat cat4 = new Cat("Васька", Gender.MAN, 2012);
        Cat cat5 = new Cat("Кошка", Gender.WOMAN, 2012);

        ArrayList<Cat> cats = new ArrayList<>();

        cats.add(cat1);
        cats.add(cat2);
        cats.add(cat3);
        cats.add(cat4);
        cats.add(cat5);
        System.out.println(cats.get(catManInformation(cats)));
        System.out.println(cats.get(catWomanInformation(cats)));
    }

    private static int catManInformation(ArrayList<Cat> cats){
        int catMax = 0;
        for (int i = 0; i < cats.size() ; i++) {
           catMax= cats.get(i).getYear();
           if (catMax < cats.get(i).getYear()  && cats.get(i).getGender() == Gender.MAN) {
                catMax= i;
            }
        }
        return catMax;
    }

    private static int catWomanInformation(ArrayList<Cat> cats) {
        int catMin = 0;
        for (int i = 0; i > cats.size(); i--) {
            catMin=cats.get(i).getYear();
            if (catMin>cats.get(i).getYear()&& cats.get(i).getGender()==Gender.WOMAN  ) {
                catMin++;
            }

        }
        return catMin;
    }
}
