package ru.pia.book;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * В классе main создаём пять книг с исходными данными
     * ArrayList - Создали массив и положили в него книги которые инициализировали
     * printMessageBooks - выводит информацию сравниваемых книгах
     * printComparison - Выводит информацию о книге если она 2018 - го года издания
     */

    public static void main(String[] args) {
        Book book1 = new Book("Пушкин", "Онегин", 2018);
        Book book2 = new Book("Лермонтов", "Бородино", 2019);
        Book book3 = new Book("Толстой", "Война и мир", 2018);
        Book book4 = new Book("Пушкин", "Капитанская дочка", 2011);
        Book book5 = new Book("Достоевский", "Преступление и наказание", 2007);


        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
       // books.add(input());
        printMessageBooks(books);
        printComparison(books);
    }


    /**
     * определяем, равен ли год издания у 1 и 2 книги
     * @param books массив книг
     */

    private static void printMessageBooks(ArrayList<Book> books) {
        int booksSize = books.size();
        if (books.get(0).isYears(books.get(booksSize - 4))) {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 4).getName() + " - равны ");
        } else {
            System.out.println("Год издания книги - " + books.get(0).getName() + "- и книги - " + books.get(booksSize - 4).getName() + " - не равны");
        }
    }

    /**
     * printComparison - приравнивает сравниваемые книги с 2018-м годом и выводим информацию если год 2018.
     *
     * @param books - Использую массив книг.
     */

    private static void printComparison(ArrayList<Book> books) {
        for (Book book : books) {
            if (book.getYear() == 2018) {
                System.out.println("Название книги - " + book.getName() + " - 2018-го года издания \n" + book);

            }
        }
    }
}