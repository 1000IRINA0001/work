package ru.pia.Сountry;


import ru.pia.Country.Country;

import java.util.ArrayList;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        /**
         * Ввела исходные данные
         * Добавила устройство ввода количества стран, необходимых для ввода
         * Создала ArrayList Страны
         *
         */
        Country russia = new Country("Россия","Москва",300,500);
        System.out.print("Сколько стран вы хотите ввести: ");
        int num =scanner.nextInt();
        ArrayList <Country> countries = new ArrayList<>();
        for (int i = 0; i < num ; i++) {
            countries.add(input());
        }
        countries.add(russia);
        System.out.println("Самая населенная страна: " + countries.get(bigPopulation(countries)));
    }

    /**
     * создала консольный ввод для названия, столицы, прлощади, населения страны
     * создала объект Country с параметрами страны
     */
    private static Country input(){
        System.out.print("Введите название: ");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.print("Введите столицу: ");
        String capital = scanner.nextLine();
        System.out.print("Введите площадь: ");
        double area = scanner.nextDouble();
        System.out.print("Введите навеление:");
        int population = scanner.nextInt();
        return new Country(name,capital,area,population);
    }

    /**
     * метод для поиска самой густонаселенной страны
     * @param countries массив стран
     * @return
     */
    private static int bigPopulation (ArrayList <Country> countries){
        int countriesSize = countries.size();
        double max = 0;
        int part = 0;

        for (int i = 0; i < countriesSize; i++) {
            double bigPopulation = countries.get(i).bigPopulation();
            if (max < bigPopulation) {
                part = i;
                max = bigPopulation;
            }
        }
        return part;
    }
}