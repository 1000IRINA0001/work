 package ru.pia.box;
public class Box {

    private String name;
    private int width;
    private int height;
    private int depth;

    public Box(String name, int width, int height, int depth) {
        this.name = name;
        //обращение к полю класса= относится к текущ. объекту
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Box(String name, int side) {
        this(name, side, side, side);
    }

    public Box() {
        this("коробочка", 1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return name +
                " width=" + width +     //операция конкотинации
                ", height=" + height +
                ", depth=" + depth +
                '}';
    }

    public int volume() {
        return width * height * depth;
    }
   // public static void outArrayOfBox(Box[] cubes) {
     //   for (int i = 0; i < cubes.length; i++) {
       //     System.out.println((i + 1) + " коробочка " + cubes[i]);


      //public static int Max( int[] volume){
        //    int max = volume[1];
        //for (int i = 0; i <volume.length; i++) {
           // if (max > volume[i]) {

             //   System.out.println("Warm day: " + (i+1));

            }

   // объект; класс; объектно ориентированное