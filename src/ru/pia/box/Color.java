package ru.pia.box;

public enum Color {

    WHITE,
    YELLOW,
    RED,
    BLUE,
    BLACK,
    GREEN
}
