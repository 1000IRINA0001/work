package ru.pia.box;

public class HeavyBox extends ColorBox {
    private double weight;

    public HeavyBox(String name, int width, int height, int depth, Color color, double weight) {
        super(name, width, height, depth, color);
        this.weight = weight;
    }


   // HeavyBox(){
     //   this("тяжелая");




    double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "HeavyBox{" +
                "weight=" + weight +
                '}';
    }
}
