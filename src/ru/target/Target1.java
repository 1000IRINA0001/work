package ru.target;
import java.util.Scanner;
public class Target1 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double x;
        double y;
        double r;
        System.out.print("Задайте значение x:");
        x = scanner.nextDouble();
        System.out.print("Задайте значение y:");
        y = scanner.nextDouble();
        System.out.print("Задайте значение r:");
        r = scanner.nextDouble();

        if ((x * x + y * y) <= (r * r)) {
            System.out.print("Попал");
        }
        if ((x * x + y * y) > (r * r)) {
            System.out.print("Мимо");
        }
    }
}
