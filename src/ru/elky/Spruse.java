package ru.elky;



public class Spruse {


    private double size;
    private Material material;

    public Spruse(double size, Material material) {
        this.size = size;
        this.material = material;
    }

    public double getSize() {
        return size;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Spruse{" +
                "size=" + size +
                ", material=" + material +
                '}';
    }



}

